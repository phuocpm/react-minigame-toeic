import React, { Component } from 'react'

export default class ContentMiddle extends Component {
    render() {
        return (
            <div className="page-content__middle">
                <div className="thumb">
                    <div className="image">
                        <img src="img/process.png" alt="lo trinh hoc toeic" />
                    </div>
                    <a href="https://www.anhngumshoa.com/tin-tuc/khoa-800-toeic-tron-doi-cam-ket-dau-ra-100-37536.html"
                        className="link link-pre" title="khoa hoc 800 toeic" />
                    <a href="https://www.anhngumshoa.com/tin-tuc/khoa-toeic-650-37803.html"
                        className="link link-toeica" title="khoa hoc 600-650 toeic" />
                    <a href="https://www.anhngumshoa.com/tin-tuc/khoa-hoc-toeic-450-500-cho-nguoi-mat-goc-tai-anh-ngu-ms-hoa-37381.html" className="link link-toeicb"
                        title="khoa hoc 450-500 toeic" />
                    <a href="https://www.anhngumshoa.com/tin-tuc/khoa-hoc-toeic-450-500-cho-nguoi-mat-goc-tai-anh-ngu-ms-hoa-37381.html"
                        className="link link-800" title="khoa hoc 450-500 toeic" />
                </div>
            </div>
        )
    }
}
