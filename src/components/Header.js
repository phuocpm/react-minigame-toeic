import React, { Component } from 'react'

export default class Header extends Component {
    render() {
        return (
            <div className="header">
                <div className="logo">
                    <a href="https://www.anhngumshoa.com/" title="anh ngu ms hoa" className="image">
                        <img src="img/logo.png" alt="logo-anhngumshoa" />
                    </a>
                </div>
                <div className="brand">
                    <div className="title">
                        TRUNG TÂM ĐÀO TẠO TOEIC SỐ 1 VIỆT NAM
                                </div>
                    <div className="sub-title">Hành trình truyền cảm hứng <span>8 năm - 16 cơ sở</span></div>
                </div>
            </div>
        )
    }
}
