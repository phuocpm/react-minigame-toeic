import React, { Component } from 'react'

export default class ContentLower extends Component {
    render() {
        return (
            <div className="page-content__lower">
                <div className="section-branch">
                    <div className="heading">
                        HỆ THỐNG CƠ SỞ LUYỆN THI toeic
                                </div>
                    <div className="sub-heading">
                        Anh ngữ Ms Hoa - Đào tạo TOEIC số 1 Việt Nam
                                </div>
                    <div className="branch-list">
                        <div className="branch">
                            <div className="branch-child">
                                <div className="title">HÀ NỘI:</div>
                                <div className="list-branch-child">
                                    <p>CS1: 41 Tây Sơn, Q. Đống Đa</p>
                                    <p>CS2: 461 Hoàng Quốc Việt, Q. Cầu Giấy</p>
                                    <p>CS3: 141 Bạch Mai, Q. Hai Bà Trưng</p>
                                    <p>CS4: 40 Nguyễn Hoàng, Mỹ Đình</p>
                                    <p>CS5: LK 6, Nguyễn Văn Lộc, Hà Đông</p>
                                    <p>CS6: 18 Nguyễn Văn Cừ, Long Biên</p>
                                </div>
                            </div>
                        </div>
                        <div className="branch">
                            <div className="branch-child">
                                <div className="title">HỒ CHÍ MINH:</div>
                                <div className="list-branch-child">
                                    <p>CS7: 569 Sư Vạn Hạnh, P13, Q10</p>
                                    <p>CS8: 49A Phan Đăng Lưu, P3, Q.Bình Thạnh</p>
                                    <p>CS9: 82 Lê Văn Việt, Hiệp Phú, Q9</p>
                                    <p>CS10: 427 Cộng Hòa, P15, Q Tân Bình</p>
                                    <p>CS11: 224 Khánh Hội, P6, Q4</p>
                                    <p>CS12: 18 Phan Văn Trị, Q. Gò Vấp</p>
                                    <p>CS13: 215 Kinh Dương Vương, Phường 12, Q6</p>
                                </div>
                            </div>
                        </div>
                        <div className="branch">
                            <div className="branch-child">
                                <div className="title">ĐÀ NẴNG:</div>
                                <div className="list-branch-child">
                                    <p>CS14: 233 Nguyễn Văn Linh, Thanh Khê</p>
                                    <p>CS15: 254 Tôn Đức Thắng, Q. Liên Chiểu</p>
                                </div>
                            </div>
                            <div className="branch-child">
                                <div className="title">HẢI PHÒNG</div>
                                <div className="list-branch-child">
                                    <p>CS16: 428 Lạch Tray, Ngô Quyền</p>
                                </div>
                            </div>
                            <div className="list-social">
                                <a href="https://www.facebook.com/mshoatoeic" title="anh ngu ms hoa">
                                    <svg aria-hidden="true" focusable="false" data-prefix="fab"
                                        data-icon="facebook-square"
                                        className="svg-inline--fa fa-facebook-square fa-w-14" role="img"
                                        xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                                        <path fill="currentColor"
                                            d="M400 32H48A48 48 0 0 0 0 80v352a48 48 0 0 0 48 48h137.25V327.69h-63V256h63v-54.64c0-62.15 37-96.48 93.67-96.48 27.14 0 55.52 4.84 55.52 4.84v61h-31.27c-30.81 0-40.42 19.12-40.42 38.73V256h68.78l-11 71.69h-57.78V480H400a48 48 0 0 0 48-48V80a48 48 0 0 0-48-48z">
                                        </path>
                                    </svg>
                                </a>
                                <a href="https://www.anhngumshoa.com/" title="anh ngu ms hoa">
                                    <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="globe"
                                        className="svg-inline--fa fa-globe fa-w-16" role="img"
                                        xmlns="http://www.w3.org/2000/svg" viewBox="0 0 496 512">
                                        <path fill="currentColor"
                                            d="M336.5 160C322 70.7 287.8 8 248 8s-74 62.7-88.5 152h177zM152 256c0 22.2 1.2 43.5 3.3 64h185.3c2.1-20.5 3.3-41.8 3.3-64s-1.2-43.5-3.3-64H155.3c-2.1 20.5-3.3 41.8-3.3 64zm324.7-96c-28.6-67.9-86.5-120.4-158-141.6 24.4 33.8 41.2 84.7 50 141.6h108zM177.2 18.4C105.8 39.6 47.8 92.1 19.3 160h108c8.7-56.9 25.5-107.8 49.9-141.6zM487.4 192H372.7c2.1 21 3.3 42.5 3.3 64s-1.2 43-3.3 64h114.6c5.5-20.5 8.6-41.8 8.6-64s-3.1-43.5-8.5-64zM120 256c0-21.5 1.2-43 3.3-64H8.6C3.2 212.5 0 233.8 0 256s3.2 43.5 8.6 64h114.6c-2-21-3.2-42.5-3.2-64zm39.5 96c14.5 89.3 48.7 152 88.5 152s74-62.7 88.5-152h-177zm159.3 141.6c71.4-21.2 129.4-73.7 158-141.6h-108c-8.8 56.9-25.6 107.8-50 141.6zM19.3 352c28.6 67.9 86.5 120.4 158 141.6-24.4-33.8-41.2-84.7-50-141.6h-108z">
                                        </path>
                                    </svg>
                                </a>
                                <a href="https://www.youtube.com/user/baigiangmshoatoeic" title="anh ngu ms hoa">
                                    <svg aria-hidden="true" focusable="false" data-prefix="fab" data-icon="youtube"
                                        className="svg-inline--fa fa-youtube fa-w-18" role="img"
                                        xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                                        <path fill="currentColor"
                                            d="M549.655 124.083c-6.281-23.65-24.787-42.276-48.284-48.597C458.781 64 288 64 288 64S117.22 64 74.629 75.486c-23.497 6.322-42.003 24.947-48.284 48.597-11.412 42.867-11.412 132.305-11.412 132.305s0 89.438 11.412 132.305c6.281 23.65 24.787 41.5 48.284 47.821C117.22 448 288 448 288 448s170.78 0 213.371-11.486c23.497-6.321 42.003-24.171 48.284-47.821 11.412-42.867 11.412-132.305 11.412-132.305s0-89.438-11.412-132.305zm-317.51 213.508V175.185l142.739 81.205-142.739 81.201z">
                                        </path>
                                    </svg>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
