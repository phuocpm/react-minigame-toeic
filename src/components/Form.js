import React, { Component } from 'react'
import Axios from 'axios';

export default class Form extends Component {
    constructor(props) {
        super(props);
        this.state = {
            username: '',
            phone: '',
            code_gift: '',
            branch: '',
            branch_name: '',
            is_loading: false,
            data_branch: [],
            data_gift: []
        }
    }

    onHandleChange = (event) => {
        // console.log(event.target.value);
        let target = event.target;
        let name = target.name;
        let value = target.value;
        this.setState({
            [name]: value,
        })
    }

    onHandleSubmit = (event) => {
        event.preventDefault();
        // console.log(this.state);

        // fetch('http://vongquaymayman.anhngumshoa.com/add_contact', {
        fetch('https://5f1aab8a610bde0016fd2df3.mockapi.io/userInfo', {
            method: 'POST',
            // We convert the React state to JSON and send it as the POST body
            body: JSON.stringify(this.state)
        }).then(function (response) {
            // console.log(response)
            return response.json();
        });
    }

    _getDataBranch = async () => {
        if (this.state.is_loading) { return }

        this.setState({
            is_loading: true
        })
        const response = await Axios.get("https://staging.api.f6.com.vn/org/branch/list?brand_id=2")
        // const response = await Axios.get("https://5f1aab8a610bde0016fd2df3.mockapi.io/listBranch")
        console.debug(response)
        if (!response.data?.error) {
            this.setState({
                is_loading: false,
                data_branch: response.data
            })
        } else {
            console.debug("Get fail")
            this.setState({
                is_loading: false
            })
        }
    }


    _getDataGift = async () => {
        if (this.state.is_loading) { return }

        this.setState({
            is_loading: true
        })

        const response = await Axios.get("https://5f1aab8a610bde0016fd2df3.mockapi.io/listGift")
        console.debug(response)
        if (!response.data?.error) {
            this.setState({
                is_loading: false,
                data_gift: response.data
            })
        } else {
            console.debug("Get fail")
            this.setState({
                data_gift: false
            })
        }
    }

    componentDidMount() {
        this._getDataBranch()
        this._getDataGift()
    }

    render() {
        // let { username, phone, code_gift, branch } = this.state;
        return (
            <form className="form-register" onSubmit={this.onHandleSubmit}>
                <div className="row">
                    <div className="col-sm-6 col-12">
                        <input
                            type="text"
                            className="form-control"
                            placeholder="Họ và tên"
                            name="username"
                            // value=
                            onChange={this.onHandleChange}
                        />
                    </div>
                    <div className="col-sm-6 col-12">
                        <input
                            type="text"
                            className="form-control"
                            placeholder="Số điện thoại"
                            name="phone"
                            onChange={this.onHandleChange}
                        />
                    </div>
                </div>
                <div className="row">
                    <div className="col-sm-6 col-12">
                        <input
                            type="text"
                            className="form-control"
                            placeholder="Mã quà tặng"
                            name="code_gift"
                            onChange={this.onHandleChange}
                        />
                        {this.state.data_gift.code_gift}
                    </div>
                    <div className="col-sm-6 col-12">
                        <select
                            id="inputState"
                            className="form-control"
                            name="branch"
                            onChange={this.onHandleChange}
                        >
                            <option value={0}>Chọn cơ sở gần bạn nhất</option>
                            {
                                this.state.data_branch.map((item, index) => {
                                    return <option key={item.branch_id} value={item.branch_id}>{item.name}</option>
                                })
                            }
                        </select>
                    </div>
                </div>
                <div className="row">
                    <div className="col">
                        <button type="submit" className="btn btn-warning" > nhận quà ngay</button>
                    </div>
                </div>
            </form>
        )
    }
}
