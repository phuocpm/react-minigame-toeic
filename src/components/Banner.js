import React, { Component } from 'react'

export default class Banner extends Component {
    render() {
        return (
            <div className="sec-left">
                <div className="image">
                    <img src="img/banner.png" alt="banner-gift" />
                </div>
                <div className="com-title">
                    <div className="title">
                        100% QUAY LÀ TRÚNG!
                                            </div>
                    <div className="sub-title">
                        Tổng giá trị giải thưởng lên tới <span>200.000.000đ</span>
                    </div>
                </div>
            </div>
        )
    }
}
